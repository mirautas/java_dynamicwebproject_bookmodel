
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="includes/bootstrapMeta.jsp" />
<title>Books</title>
<jsp:include page="includes/bootstrapCss.jsp" />
<link
	href="http://www.malot.fr/bootstrap-datetimepicker/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css"
	rel="stylesheet">
</head>
<body>
	<div class="container" role="main">

		<!--  add or edit?  ----------------------------------------------------------- -->
		<c:choose>
			<c:when test="${not empty book}">
				<c:set var="legend">Change Book ${book.uid}</c:set>
				<c:set var="formAction">changeBook</c:set>
				<c:set var="readonly">readonly</c:set>
			</c:when>
			<c:otherwise>
				<c:set var="legend">New Book</c:set>
				<c:set var="formAction">addBook</c:set>
				<c:set var="readonly"></c:set>
			</c:otherwise>
		</c:choose>
		<!--  add or edit?  ----------------------------------------------------------- -->

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form class="form-horizontal" method="post" action="${formAction}">
					<fieldset>
						<legend>${legend}</legend>

						<! ----------------  uid ---------------- -->
						<div class="form-group">
							<label for="inputUid" class="col-md-2 control-label">Uid</label>
							<div class="col-md-10">
								<input class="form-control" id="inputUid" type="text" name="uid"
									${readonly} value="<c:out value="${book.uid}"/>">
							</div>
						</div>

						<! ----------------  Title ---------------- -->
						<div class="form-group">
							<label for="inputTitle" class="col-md-2 control-label">Title</label>
							<div class="col-md-10">
								<input class="form-control" id="inputTitle" type="text"
									name="bookTitle" value="<c:out value="${book.bookTitle}"/>">
							</div>
						</div>
						<! ----------------  author ---------------- -->
						<div class="form-group">
							<label for="inputAuthor" class="col-md-2 control-label">Author</label>
							<div class="col-md-10">
								<input class="form-control" id="inputAuthor" type="text"
									name="bookAuthor" value="<c:out value="${book.bookAuthor}"/>">
							</div>
						</div>

						<! ----------------  publicationDate ---------------- -->
						<div class="form-group">
							<label for="inputDate" class="col-md-2 control-label">Date</label>
							<div class="col-md-10">
								<input class="form_datetime" id="inputDate" placeholder="Date"
									type="text" readonly name="publicationDate"
									value="<fmt:formatDate value="${book.publicationDate}" pattern="dd.MM.yyyy"/>">
							</div>
						</div>

						<! ----------------  buttons ---------------- -->
						<div class="form-group">
							<div class="col-md-10 col-md-offset-2">
								<button type="submit" class="btn btn-primary">Submit</button>
								<a href="listBooks">
									<button type="button" class="btn btn-default">Cancel</button>
								</a>
							</div>
						</div>

					</fieldset>
				</form>
			</div>
		</div>

	</div>
	<!--  End of container -->


<!-- JS for Bootstrap -->
	<%@include file="includes/bootstrapJs.jsp"%>
<!-- JS for Bootstrap -->


<!-- JS for Datetime picker -->

	<script type="text/javascript"
		src="http://www.malot.fr/bootstrap-datetimepicker/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

	<script>
		$(function() {

			$(".form_datetime").datetimepicker({
				format : "dd.mm.yyyy",
				autoclose : true,
				todayBtn : true,
				pickerPosition : "bottom-left",
				minView : 2
			});

		});
	</script>

</body>
</html>