package net.netconomy.booklist.model;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

public class BookModel {

	@Min(1)
	private int uid;

	private String bookTitle;
	private String bookAuthor;

	@Past(message = "must be in the past")
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	private Date publicationDate;

	public BookModel(@Min(1) int uid, String bookTitle, String bookAuthor,
			@Past(message = "must be in the past") Date publicationDate) {
		super();
		this.uid = uid;
		this.bookTitle = bookTitle;
		this.bookAuthor = bookAuthor;
		this.publicationDate = publicationDate;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}



	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + uid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookModel other = (BookModel) obj;
		if (uid != other.uid)
			return false;
		return true;
	}

}
