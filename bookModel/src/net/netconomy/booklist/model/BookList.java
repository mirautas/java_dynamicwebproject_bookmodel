package net.netconomy.booklist.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;


@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class BookList {

	private List<BookModel> books = new ArrayList<BookModel>();

	/**
	 * Add Book to List
	 *
	 * @param Book
	 */
	public void addBook(BookModel book) {
		books.add(book);
	}

	/**
	 * Verify if list contains Book with same uid
	 *
	 * @param Book
	 * @return
	 */
	public boolean contains(BookModel book) {
		return books.contains(book);
	}

	/**
	 * convenient method: true if list is empty
	 *
	 * @return
	 */
	public boolean isEmpty() {
		return books.isEmpty();
	}

	/**
	 * try to find BookModel with given uid return model, otherwise null
	 *
	 * @param uid
	 * @return
	 */
	public BookModel getBookByUid(int uid) {
		Optional<BookModel> book = books.stream().filter(bookModel -> bookModel.getUid() == uid).findFirst();
		return book.orElse(null);
	}

	/**
	 * return list with all books
	 *
	 * @return
	 */
	public List<BookModel> getAllBooks() {
		return books;
	}

	/**
	 * return a new list with all books where title or author contains search
	 * string
	 *
	 * @param searchString
	 * @return
	 */
	public List<BookModel> getFilteredBooksJava7(String searchString) {

		// empty List for results
		List<BookModel> filteredList = new ArrayList<BookModel>();
		for (BookModel bookModel : books) {
			if ((bookModel.getBookTitle() != null && bookModel.getBookTitle().contains(searchString))
					|| (bookModel.getBookAuthor() != null && bookModel.getBookAuthor().contains(searchString))) {
				filteredList.add(bookModel);
			}
		}
		return filteredList;
	}

	/**
	 * return a new list with all books where title or author contains search
	 * string
	 *
	 * @param searchString
	 * @return
	 */
	public List<BookModel> getFilteredBooks(String searchString) {
		if (searchString == null || searchString.equals("")) {
			return books;
		}
		// check every Book
		return books.stream().filter(bookModel -> {
			return checkBook(bookModel, searchString);
		}).collect(Collectors.toList());
	}

	private boolean checkBook(BookModel bookModel, String searchString) {
		return ((bookModel.getBookTitle() != null && bookModel.getBookTitle().contains(searchString))
				|| (bookModel.getBookAuthor() != null && bookModel.getBookAuthor().contains(searchString)));
	}

	/**
	 * remove books with same uid
	 *
	 * @param uid
	 * @return
	 */
	public boolean removeBook(int uid) {
		return books.remove(new BookModel(uid, null, null, null));
	}
}
