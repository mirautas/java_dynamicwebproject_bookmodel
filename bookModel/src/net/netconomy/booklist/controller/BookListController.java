package net.netconomy.booklist.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.netconomy.booklist.model.BookList;
import net.netconomy.booklist.model.BookModel;

@Controller
public class BookListController {

	@Autowired
	private BookList bookList;

	@RequestMapping(value = { "/", "listBooks" })
	public String showAllBooks(Model model) {
		model.addAttribute("books", bookList.getAllBooks());
		return "listBooks";
	}

	@RequestMapping("/fillBookList")
	public String fillBookList() {
		Date now = new Date();
		bookList.addBook(new BookModel(1, "In der Nacht h�r� ich die Sterne", "Paolla Peretti", now));
		bookList.addBook(new BookModel(2, "The End", "Eric Wrede", now));
		bookList.addBook(new BookModel(3, "Becomming", "Michelle Obama", now));
		bookList.addBook(new BookModel(4, "Gregs Tagebuch 13 - Eiskalt erwischt", "Jeff Kinney", now));
		bookList.addBook(new BookModel(5, "Der Apfelbaum", "Christian Berkel", now));
		bookList.addBook(new BookModel(6, "Die Suche", "Charlotte Link", now));
		return "forward:/listBooks";
	}

	// Spring 4: @RequestMapping(value = "/ deleteBook ", method =
	// RequestMethod.GET)
	@GetMapping("/deleteBook")
	public String delete(Model model, @RequestParam int uid) {
		boolean isRemoved = bookList.removeBook(uid);
		if (isRemoved) {
			model.addAttribute("warningMessage", "Book " + uid + " deleted");
		} else {
			model.addAttribute("errorMessage", "There is no Book " + uid);
		}
		// Multiple ways to "forward" to another Method
		// return "forward:/listBooks";
		return showAllBooks(model);
	}

	@PostMapping("/searchBooks")
	public String search(Model model, @RequestParam String searchString) {
		model.addAttribute("books", bookList.getFilteredBooks(searchString));
		return "listBooks";
	}

	@RequestMapping(value = "/addBook", method = RequestMethod.GET)
	public String showAddBookForm(Model model) {
		return "editBook";
	}

	@RequestMapping(value = "/addBook", method = RequestMethod.POST)
	public String addBook(@Valid @ModelAttribute BookModel newBookModel, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			String errorMessage = "";
			for (FieldError fieldError : bindingResult.getFieldErrors()) {
				errorMessage += fieldError.getField() + " is invalid<br>";
			}
			model.addAttribute("errorMessage", errorMessage);
			return "forward:/listBooks";
		}
		BookModel book = bookList.getBookByUid(newBookModel.getUid());
		if (book != null) {
			model.addAttribute("errorMessage", "Book already exists!<br>");
		} else {
			bookList.addBook(newBookModel);
			model.addAttribute("message", "New book " + newBookModel.getUid() + " added.");
		}
		return "forward:/listBooks";
	}

	@RequestMapping(value = "/changeBook", method = RequestMethod.GET)
	public String showChangeBookForm(Model model, @RequestParam int uid) {
		BookModel book = bookList.getBookByUid(uid);
		if (book != null) {
			model.addAttribute("book", book);
			return "editBook";
		} else {
			model.addAttribute("errorMessage", "Couldn't find book " + uid);
			return "forward:/listBooks";
		}
	}

	@RequestMapping(value = "/changeBook", method = RequestMethod.POST)
	public String changeBook(@Valid @ModelAttribute BookModel changedBookModel, BindingResult bindingResult,
			Model model) {
		if (bindingResult.hasErrors()) {
			String errorMessage = "";
			for (FieldError fieldError : bindingResult.getFieldErrors()) {
				errorMessage += fieldError.getField() + " is invalid:" + fieldError.getDefaultMessage() + "<br>";
			}
			model.addAttribute("errorMessage", errorMessage);
			return "forward:/listBooks";
		}
		BookModel book = bookList.getBookByUid(changedBookModel.getUid());
		if (book == null) {
			model.addAttribute("errorMessage", "Book does not exist!<br>");
		} else {
			book.setUid(changedBookModel.getUid());
			book.setBookTitle(changedBookModel.getBookTitle());
			book.setBookAuthor(changedBookModel.getBookAuthor());
			book.setPublicationDate(changedBookModel.getPublicationDate());
			model.addAttribute("message", "Changed book " + changedBookModel.getUid());
		}
		return "forward:/listBooks";
	}

	@ExceptionHandler(Exception.class)
	public String handleAllException(Exception ex) {
		return "showError";

	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(
	            dateFormat, false));
	}

}
